#!/bin/bash
# A post-simulation script to reconstruct decomposed
# simulations and generate derived property fields
#
# Read carefully before running: you should see this 
# file more like a memo/TODO list than a strict set of
# instructions to be run blindly


#######################################################
# Preliminaries
#######################################################

# nifty block commenting shortcut
[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then" # (test never is true)
alias ENDCOMMENT="fi"


#######################################################
# Post- simulation
#######################################################

# Keep only the last 20 000 lines of the simulation log
tail -20000 logs/14_simulationLog > logs/14_simulationLogTail

# recompose calculation domain from the 6 CPU folders:
reconstructPar -newTimes

# optional:
# extract data from the solution output
# configuration is defined in file system/sampleDict:
# sample

# generate fields derived from basic properties
yPlusRAS
wallShearStress
vorticity
Pe # Péclet
Co # Courant (CFL)

notify-send -i gnome-terminal --hint int:transient:1 "Done" "Script completed"
