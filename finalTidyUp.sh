#!/bin/bash
# Script to remove temporary/work files from case.
# Should not affect simulation results.
#
# Read carefully before running: you should see this 
# file more like a memo/TODO list than a strict set of
# instructions to be run blindly


#######################################################
# Preliminaries
#######################################################

# nifty block commenting shortcut
[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then" # (test never is true)
alias ENDCOMMENT="fi"


#######################################################
# Tidying up
#######################################################

# Now that the case has beeen recomposed, delete the contents of the processor folders,
# to save disk space.
rm -rf processor* | tee --append logs/15_tidyupLog


# Move the pyFoam residue log files to a storage folder
mv --verbose PyFoam* logs/ | tee --append logs/15_tidyupLog
mv --verbose PlyParser_FoamFileParser_parsetab.py logs/ | tee --append logs/15_tidyupLog

rm -r logs/Decomposer.analyzed logs/Gnuplotting.analyzed
mv --verbose Decomposer.logfile logs/ | tee --append logs/15_tidyupLog
mv --verbose Decomposer.analyzed logs/ | tee --append logs/15_tidyupLog
mv --verbose Gnuplotting.analyzed logs/ | tee --append logs/15_tidyupLog

notify-send -i gnome-terminal --hint int:transient:1 "Done" "Script completed"
