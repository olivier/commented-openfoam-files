#!/bin/bash
# Script to run the simulation itself 
#
# Read carefully before running: you should see this 
# file more like a memo/TODO list than a strict set of
# instructions to be run blindly


#######################################################
# Preliminaries
#######################################################

# nifty block commenting shortcut
[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then" # (test never is true)
alias ENDCOMMENT="fi"


#######################################################
# Preparation
#######################################################

# Create folder where logs will be stored
mkdir --verbose --parents logs/old # --parents: do not complain if already existing
mv --verbose logs/12_boundaryConditionCleanupLog logs/old/

# turn compression on and change format to binary
sed --in-place 's/^writeCompression.*/writeCompression\tcompressed;/' system/controlDict
sed --in-place 's/^writeFormat.*/writeFormat\t\t\tbinary;/' system/controlDict



#######################################################
# Simulation
#######################################################

# decompose calculation domain to allow computation with 6 CPUS:
pyFoamDecompose.py . 6 | tee --append logs/13_decompositionLog

# THE MAIN SIMULATION -- OYE
# naturally, replace interDyMFoam by your favorite solver  here
pyFoamPlotRunner.py --autosense-parallel interDyMFoam -case . | tee --append logs/14_simulationLog
# or simply single-core computation:
#interDyMFoam | tee --append logs/14_simulationLog


notify-send -i gnome-terminal --hint int:transient:1 "Done" "Script completed"
