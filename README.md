Commented OpenFOAM files
========================

This is a simple set of OpenFOAM configuration files and run scripts, commented for easier understanding. They are used to carry out simulations with OpenFOAM 2.3.1 and make use of the excellent <a href="https://pypi.python.org/pypi/PyFoam">PyFOAM</a> toolbox.

## License

These files are released into the public domain with the <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.en">CC-0</a> dedication. No warranty of any kind is associated to the use of these tools.
