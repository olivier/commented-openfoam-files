#!/bin/bash
# Script to run a potential flow (initialization)
# simulation. 
# Note that potentialFoam solvers require relatively
# simple boundary conditions to run. This script is
# intended to be run in a similar-but-separate case
# folder, and its contents imported back into the real
# case with the importPotentialFlowVelocityField.sh
# script.
#
# Read carefully before running: you should see this 
# file more like a memo/TODO list than a strict set of
# instructions to be run blindly


#######################################################
# Preliminaries
#######################################################

# nifty block commenting shortcut
[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then" # (test never is true)
alias ENDCOMMENT="fi"


#######################################################
# Change initial conditions to fit potentialFOAM solver
#######################################################

# quick backup
mv --verbose 0/U 0/backup/U.beforePotentialFlow | tee --append logs/11_potentialFlowInitLog
mv --verbose 0/p 0/backup/p.beforePotentialFlow | tee --append logs/11_potentialFlowInitLog
rm --verbose 0/U.gz | tee --append logs/11_potentialFlowInitLog

# move special potentialFlow propertyfiles to 0/
# Essentially, these differ from the "real" case files in the outlet boundary conditions, which are simply set to zerogradient
cp --verbose 0.org/p.potentialFlow 0/p | tee --append logs/11_potentialFlowInitLog
cp --verbose 0.org/U.potentialFlow 0/U | tee --append logs/11_potentialFlowInitLog

# change turbulence modelling from RANS to laminar
cp --verbose constant/turbulenceProperties.potentialFlow constant/turbulenceProperties | tee --append logs/11_potentialFlowInitLog

# make sure compression is off and that format is ascii
sed --in-place 's/^writeCompression.*/writeCompression\tuncompressed;/' system/controlDict
sed --in-place 's/^writeFormat.*/writeFormat\tascii;/' system/controlDict


#######################################################
# Run solver
#######################################################

potentialFoam | tee --append logs/11_potentialFlowInitLog


#######################################################
# Cleanup after solver
#######################################################

# get rid of useless p field in 0/ folder
mv --verbose 0/p 0/backup/p.afterPotentialFlow | tee --append logs/11_potentialFlowInitLog

# switch turbulence modelling on again
cp --verbose constant/turbulenceProperties.realisticFlow constant/turbulenceProperties | tee --append logs/11_potentialFlowInitLog

notify-send -i gnome-terminal --hint int:transient:1 "Done" "Script completed"
