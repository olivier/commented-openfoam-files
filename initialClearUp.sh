#!/bin/bash
# Script to clear the case of its property fields and 
# mesh
#
# Read carefully before running: you should see this 
# file more like a memo/TODO list than a strict set of
# instructions to be run blindly


#######################################################
# Preliminaries
#######################################################

# nifty block commenting shortcut
[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then" # (test never is true)
alias ENDCOMMENT="fi"


#######################################################
# Cleanup
#######################################################

# Create folder where logs will be stored
mkdir --verbose --parents logs/old # --parents: do not complain if already existing
rm --verbose logs/old/*
mv --verbose logs/* logs/old/ | tee --append logs/0_cleanupLog

# Clear case entirely
# Optional: use --keep-last to keep the last written time directory
pyFoamClearCase.py --verbose --processors-remove . | tee --append logs/0_cleanupLog

# Erase old backup
# Make a quick backup of files in 0/ folder
# Crush files in 0/ with files from 0.org/
mkdir --verbose --parents 0/backup | tee --append logs/0_cleanupLog
rm --verbose -rf 0/backup/* | tee --append logs/0_cleanupLog
mv --verbose 0/* 0/backup/ | tee --append logs/0_cleanupLog
rsync -avh --verbose --delete --exclude=backup --exclude=*.potentialFlow --exclude=p --exclude=*.boundaryConditions 0.org/ 0/ | tee --append logs/0_cleanupLog

notify-send -i gnome-terminal --hint int:transient:1 "Done" "Script completed"
