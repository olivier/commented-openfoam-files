#!/bin/bash
# A script to import a velocity field from a separate
# case which is generated with a potentialFoam solver
#
# Read carefully before running: you should see this 
# file more like a memo/TODO list than a strict set of
# instructions to be run blindly


#######################################################
# Preliminaries
#######################################################

# nifty block commenting shortcut
[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then" # (test never is true)
alias ENDCOMMENT="fi"


#######################################################
# Import U field from simulation-potentialinit case
#######################################################

# quick backup
cp --verbose 0/U 0/backup/U.beforePotentialFlow | tee --append logs/11_potentialFlowInitLog

# import U
mapFields -consistent -fields '(U)' ../simulation-potentialinit/

notify-send -i gnome-terminal --hint int:transient:1 "Done" "Script completed"
