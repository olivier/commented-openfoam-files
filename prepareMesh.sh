#!/bin/bash
# A script to import a velocity field from a separate
# case which is generated with a potentialFoam solver
#
# Read carefully before running: you should see this 
# file more like a memo/TODO list than a strict set of
# instructions to be run blindly


#######################################################
# Preliminaries
#######################################################

# nifty block commenting shortcut
[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then" # (test never is true)
alias ENDCOMMENT="fi"


#######################################################
# Preparation
#######################################################

# Make sure compression is off (for easier git diffs), but make sure that that format is ascii
# (in case we need to edit those files)
sed --in-place 's/^writeCompression.*/writeCompression\tcompressed;/' system/controlDict
sed --in-place 's/^writeFormat.*/writeFormat\t\t\tascii;/' system/controlDict

# Erase contents of ./constant/polyMesh
foamClearPolyMesh


#######################################################
# Mesh generation
#######################################################


#######################################################
# Base mesh creation

# Create mesh with blockMesh
# configuration is defined in system/blockMeshDict
blockMesh | tee logs/1_blockMeshLog

	# check mesh quality
	# This command may output a set of "non-orthogonal faces" in file constant/polyMesh/nonOrthoFaces
	checkMesh | tee logs/2_checkMeshLog


#######################################################
# Meshing around case geometries

# Extract geometry witgh surfaceFeatureExtract
# configuration is defined in system/surfaceFeatureDict
# 	creates an edgemesh (.eMesh) file from selected stl entries
# 	which will be used by snappyHexMesh later to work on feature edges
surfaceFeatureExtract | tee logs/3_surfaceFeatureExtractLog


# Edit mesh with snappyHexMesh
# configuration is defined in system/snappyHexMeshDict
# 	note: ommitting option -overwrite will result in the meshes being written to the new incremental time directories
#	while this allows better visualization of the snappyHexMesh process,
#	it might cause problems for extrudeMesh, which (apparently?) works only on the first time dir

# 1 Castellation:
	echo "## crushing snappyHexMeshDict with content from snappyHexMeshDict_onlycastellation" | tee --append logs/4_snappyHexMeshLog
	cp --verbose system/snappyHexMeshDict_onlycastellation system/snappyHexMeshDict | tee --append logs/4_snappyHexMeshLog
	snappyHexMesh -overwrite | tee --append logs/4_snappyHexMeshLog
	
# 2 Snapping:
	echo "## crushing snappyHexMeshDict with content from snappyHexMeshDict_onlysnapping" | tee --append logs/4_snappyHexMeshLog
	cp --verbose system/snappyHexMeshDict_onlysnapping system/snappyHexMeshDict
	snappyHexMesh -overwrite | tee --append logs/4_snappyHexMeshLog

# 3 Layering:
	echo "## crushing snappyHexMeshDict with content from snappyHexMeshDict_onlylayering" | tee --append logs/4_snappyHexMeshLog
	cp --verbose system/snappyHexMeshDict_onlylayering system/snappyHexMeshDict
	snappyHexMesh -overwrite | tee --append logs/4_snappyHexMeshLog

# Restore initial (castellation) snappyHexMeshDict
	cp --verbose system/snappyHexMeshDict_onlycastellation system/snappyHexMeshDict | tee --append logs/4_snappyHexMeshLog

	# check mesh quality again
	checkMesh | tee logs/5_checkMeshLog


#######################################################
# Mesh manipulation to make a 2D, moving-interface mesh
#######################################################


#######################################################
# Tweak boundaries

# Clear empty boundaries that were created during the snappyHexMesh routine
pyFoamClearEmptyBoundaries.py .

# Change boundary type of rightSide and leftSide: we wanted 'patch' for snappyHexMesh to work correctly,
# but our fields have them defined as 'empty' (rightly so, since this is a 2D case), and setFields and 
# spitMeshRegions will choke if we do not correct this now.
pyFoamChangeBoundaryType.py . rightSide empty
pyFoamChangeBoundaryType.py . leftSide empty


#######################################################
# Mesh splitting: prepare for mesh movement

# Create baffles (faces that belong to two cells) along selected faceZones
# configuration defined in system/createBafflesDict
createBaffles -overwrite | tee logs/6_meshSplitingLog
# duplicate the baffles, so we can attribute each side to one new zone
mergeOrSplitBaffles -split -overwrite | tee logs/6_meshSplitingLog
# voodoo coommand so that extrudeMesh will not suppress AMI / moving mesh information
splitMeshRegions -makeCellZones -overwrite | tee logs/6_meshSplitingLog


#######################################################
# Mesh extrusion: create in 2D×1 mesh instead of 3D mesh

# extrudeMesh
# configuration is defined in system/extrudeMeshDict
extrudeMesh | tee logs/7_extrudeMeshdictLog

	# check mesh quality AGAIN
	checkMesh | tee logs/8_checkMeshLog


# Print case report before we run setFields
# (summarizes boundary conditions, solvers etc.)
pyFoamCaseReport.py --full-report . | tee logs/CaseReport


#######################################################
# Initialize property fields

# setFields: initialize some properties based on volume functions, not on initial hand-written definitions in folder 0/
# configuration is defined in file system/setFieldsDict
# WARNING: running setFields will overwrite the field files (those corresponding to the properties
# affected in the setFieldsDict) in the time directory 0/ . NO AUTOMATIC BACKUPS ARE MADE!!!
setFields | tee logs/9_setFieldsLog

	# check mesh quality AGAINNNN
	checkMesh | tee logs/10_checkMeshLog


#######################################################
# Finish
#######################################################

notify-send -i gnome-terminal --hint int:transient:1 "Done" "Script completed"
